import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class OnlineFoodOrderingApp {
    private static List<FoodItem> menu = new ArrayList<>();
    private static Order currentOrder = new Order();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        initializeMenu();

        int choice;
        do {
            displayMenu();
            System.out.print("=> ");
            choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    selectFood(choice);
                    break;
                case 2:
                    selectFood(choice);
                    break;
                case 3:
                    selectFood(choice);
                    break;
                case 4:
                    selectFood(choice);
                    break;
                case 5:
                    selectFood(choice);
                    break;
                case 99:
                    viewOrder();
                case 0:
                    System.out.println("Thank you for using our service!");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        } while (choice != 6);
    }

    private static void initializeMenu() {
        menu.add(new FoodItem("Nasi Goreng", 15000));
        menu.add(new FoodItem("Mie Goreng", 13000));
        menu.add(new FoodItem("Nasi + Ayam", 18000));
        menu.add(new FoodItem("Es Teh Manis", 3000));
        menu.add(new FoodItem("Es Jeruk", 5000));
    }

    private static void displayMenu() {
        System.out.println("==========================");
        System.out.println("Selamat Datang di BinarFud");
        System.out.println("==========================");
        System.out.println();
        System.out.println("Silahkan pilih makanan :");
        System.out.println("1. Nasi Goreng  | 15.000");
        System.out.println("2. Mie Goreng   | 13.000");
        System.out.println("3. Nasi + Ayam  | 18.000");
        System.out.println("4. Es Teh Manis | 3.000");
        System.out.println("5. Es Jeruk     | 5.000");
        System.out.println("99. Pesan dan Bayar");
        System.out.println("0. Keluar Aplikasi");
    }

    private static void selectFood(int foodNumber) {
        FoodItem selectedFood = menu.get(foodNumber - 1);
        System.out.println("==========================");
        System.out.println("Berapa pesanan anda");
        System.out.println("==========================");
        System.out.println();
        System.out.println(selectedFood.getName() + "       | " + selectedFood.getPrice());
        System.out.println("(input 0 untuk kembali)");
        System.out.println();
        System.out.print("qty => ");
        int quantity = scanner.nextInt();
        if (quantity == 0) {
            return;
        } else if (quantity > 0) {
            currentOrder.addItem(selectedFood, quantity);
        } else {
            System.out.println("Invalid quantity.");
        }
    }

    private static void viewOrder() {
        List<FoodItem> items = currentOrder.getItems();
        List<Integer> quantities = currentOrder.getQuantity();

        System.out.println("==========================");
        System.out.println("Konfirmasi & Pembayaran");
        System.out.println("==========================");
        System.out.println();

        for (int i = 0; i < items.size(); i++) {
            System.out.println(items.get(i).getName() + "   " + quantities.get(i) + "   "
                    + items.get(i).getPrice() * quantities.get(i));
        }

        System.out.println("-------------------------+");
        System.out.println("Total         " + currentOrder.getTotalQuantity() + "   " + currentOrder.getTotalPrice());
        System.out.println();

        System.out.println("1. Konfirmasi dan Bayar");
        System.out.println("2. Kembali ke menu utama");
        System.out.println("3. Keluar aplikasi");
        System.out.println();
        System.out.print("=> ");

        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                structReceipt();
                generateReceipt();
                System.exit(0);
                break;
            case 2:
                currentOrder = new Order();
                break;
            case 3:
                System.out.println("Thank you for using our service!");
                System.exit(0);
            default:
                System.out.println("Invalid choice. Please try again.");
        }
    }

    private static void structReceipt() {
        System.out.println("==========================");
        System.out.println("BinarFud");
        System.out.println("==========================");
        System.out.println();
        System.out.println("Terima kasih sudah memesan di BinarFud");
        System.out.println();
        System.out.println("Dibawah ini adalah pesanan anda:");
        List<FoodItem> items = currentOrder.getItems();
        List<Integer> quantities = currentOrder.getQuantity();
        for (int i = 0; i < items.size(); i++) {
            System.out.println(items.get(i).getName() + "   " + quantities.get(i) + "   "
                    + items.get(i).getPrice() * quantities.get(i));
        }
        System.out.println("-------------------------+");
        System.out.println("Total         " + currentOrder.getTotalQuantity() + "   " + currentOrder.getTotalPrice());
        System.out.println();
        System.out.println("Pembayaran : BinarCash");
        System.out.println();
        System.out.println("==========================");
        System.out.println("Simpan struk ini sebagai bukti pembayaran");
        System.out.println("==========================");
    }

    private static void processPayment(Scanner scanner) {
        System.out.println("Total Amount: Rp" + currentOrder.getTotalPrice());
        System.out.println("Enter payment amount:");
        int payment = scanner.nextInt();
        if (payment >= currentOrder.getTotalPrice()) {
            System.out.println("Payment successful. Change: Rp" + (payment - currentOrder.getTotalPrice()));
        } else {
            System.out.println("Insufficient payment.");
        }
    }

    private static void generateReceipt() {
        try (PrintWriter writer = new PrintWriter(new FileWriter("receipt.txt"))) {
            writer.println("==========================");
            writer.println("BinarFud");
            writer.println("==========================");
            writer.println();
            writer.println("Terima kasih sudah memesan di BinarFud");
            writer.println();
            writer.println("Dibawah ini adalah pesanan anda:");
            List<FoodItem> items = currentOrder.getItems();
            List<Integer> quantities = currentOrder.getQuantity();
            for (int i = 0; i < items.size(); i++) {
                writer.println(items.get(i).getName() + "   " + quantities.get(i) + "   "
                        + items.get(i).getPrice() * quantities.get(i));
            }
            writer.println("-------------------------+");
            writer.println("Total         " + currentOrder.getTotalQuantity() + "   " + currentOrder.getTotalPrice());
            writer.println();
            writer.println("Pembayaran : BinarCash");
            writer.println();
            writer.println("==========================");
            writer.println("Simpan struk ini sebagai bukti pembayaran");
            writer.println("==========================");
        } catch (IOException e) {
            System.out.println("Failed to save the file: " + e.getMessage());
        }

    }
}
