import java.util.ArrayList;
import java.util.List;

class Order {
    private List<FoodItem> items;
    private List<Integer> quantities;

    public Order() {
        items = new ArrayList<>();
        quantities = new ArrayList<>();
    }

    public void addItem(FoodItem item, int quantity) {
        items.add(item);
        quantities.add(quantity);
    }

    public List<FoodItem> getItems() {
        return items;
    }

    public List<Integer> getQuantity() {
        return quantities;
    }

    public int getTotalQuantity() {
        int totalQuantity = 0;
        for (int quantity : quantities) {
            totalQuantity += quantity;
        }
        return totalQuantity;
    }

    public int getTotalPrice() {
        int totalPrice = 0;
        for (int i = 0; i < items.size(); i++) {
            totalPrice += items.get(i).getPrice() * quantities.get(i);
        }
        return totalPrice;
    }
}